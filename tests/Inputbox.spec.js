import { test, expect } from '@playwright/test';
test('Handle inputbox', async ({ page }) => {
    await page.goto('https://the-internet.herokuapp.com/login');
    const firstName= await page.locator("//input[@id='username']")
    await expect(firstName).toBeVisible();
    await expect(firstName).toBeEmpty();
    await firstName.fill("test Amir")
   await page.waitForTimeout(5000); // Pausing the code
});