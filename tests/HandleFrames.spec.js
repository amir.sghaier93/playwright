import{test,expect} from '@playwright/test'

test('Alert with Ok', async ({page}) =>{
await page.goto('https://ui.vision/demo/webtest/frames')
//Total frames
const allFrames= await page.frames();
console.log(allFrames.length);
// approach 1: using name or url
// const frame1= await page.frame({url:'https://ui.vision/demo/webtest/frames/frame_1.html'})
// await frame1.fill("input[name='mytext1']","Amir")

// approach 2: using frame locator
const inputbox= await page.frameLocator("frame[src='frame_1.html']").locator("input[name='mytext1']")
await inputbox.fill("hello")
await page.waitForTimeout(3000)
})