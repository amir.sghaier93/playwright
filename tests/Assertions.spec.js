import { test, expect } from '@playwright/test'; // import des fonctions test et expect
test('Assertions', async ({ page }) => {  // définition d'un cas de test
    await page.goto('https://demo.nopcommerce.com/register');
    await expect(page).toHaveURL('https://demo.nopcommerce.com/register');
    const logo = await page.locator("//img[@alt='nopCommerce demo store']");
    await expect(logo).toBeVisible();
    const searchInput = await page.locator('#small-searchterms');
    await page.waitForSelector('#small-searchterms'); // Attendre que l'élément soit chargé
    await expect(searchInput).toBeEnabled();
    await page.click('#gender-m  ale')
    await expect(page.locator('#gender-male')).toBeChecked();
    const registerTxt= await page.locator('#register-button');
    expect(registerTxt).toHaveText('Register')
    expect(registerTxt).toContainText('Reg')
const email= await page.locator("//input[@id='Email']");
await email.fill('test@demo.com');
await expect(email).toHaveValue('test@demo.com')
const options= await page.locator("//select[@name='DateOfBirthMonth']/option");
await expect(options).toHaveCount(13)
await page.pause();
});