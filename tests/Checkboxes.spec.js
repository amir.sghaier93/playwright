import { test, expect } from '@playwright/test';
test('Checkboxes', async ({ page }) => {
   await page.goto("https://the-internet.herokuapp.com/checkboxes")
   // single checkbox
   await page.check('//input[2]');
   expect(await page.locator('//input[2]')).toBeChecked();
   expect(await page.locator('//input[2]').isChecked()).toBeTruthy(); // same as the first check but in a different way
//    console.log(await page.locator('//input[1]').isChecked()).toBeTruthy();
//    expect(await page.locator('//input[1]').isChecked()).toBeTruthy(); // same as the first check but in a different way
   const checkbox=["//input[1]",
   '//input[2]'
   ];
   for(const chkbx of checkbox){
    await page.locator(chkbx).check();
   }
   for(const chkbx of checkbox){
    if(page.locator(chkbx).isChecked()){
    await page.locator(chkbx).uncheck();
}
   }
//    await page.pause();
   await page.waitForTimeout(5000); 
})