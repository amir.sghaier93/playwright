// @ts-check
import{test, expect} from '@playwright/test';
test('LocateMultipleElements', async ({page})=>{
    await page.goto('https://demoblaze.com/index.html');
    await page.waitForSelector("//div[@class='card-block']//a");
  const links=  await page.$$("//div[@class='card-block']//a");
//   console.log(typeof(links));
for (const link of links){
    console.log(await link.textContent());
}
});